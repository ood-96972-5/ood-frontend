const path = require('path')
const webpack = require('webpack')
const config = require('./webpack.base.config')
const BundleTracker = require('webpack-bundle-tracker')
const CompressionPlugin = require('compression-webpack-plugin')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin

config.mode = 'production'
config.devtool = 'source-map'
config.output.path = path.resolve('./dist')
config.plugins = config.plugins.concat([
  new BundleAnalyzerPlugin({
    analyzerMode: 'static',
    openAnalyzer: false,
  }),
  new CompressionPlugin(),
])

module.exports = config
