# OOD Project Frontend

This is the frontend for our OOD Project.

## Installing Dependencies

* Install nodejs dependencies

```bash
npm install
```

## Development Use

* To run a development environment first run

```bash
npm start
```

## Production Use

* To run a production environment run

```bash
BACKEND_URL=http://localhost:8080 ./up.sh
```

The default value of `BACKEND_URL` is `http://localhost:8080`. So, if you bind the
port of the backend to the 8080 port of your host, it will work without specifying
the `BACKEND_URL`.
