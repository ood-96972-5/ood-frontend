const path = require('path')
const webpack = require('webpack')
const config = require('./webpack.base.config')
const BundleTracker = require('webpack-bundle-tracker')

config.mode = 'development'
config.devtool = 'eval-source-map'

config.entry['react-hot'] = 'react-hot-loader/patch'
config.entry['dev-hot'] = 'webpack-dev-server/client?http://localhost:8090'
config.entry['hot'] = 'webpack/hot/only-dev-server'

config.plugins = config.plugins.concat([
  new webpack.HotModuleReplacementPlugin(),
  new webpack.NoEmitOnErrorsPlugin(),
])

config.module.rules[0].use[0].options.plugins.push("react-hot-loader/babel")
config.module.rules[1].use[0] = { loader: 'style-loader' } // Use style-loader until MiniCssExtractPlugin supports hot reload
config.module.rules[2].use[0] = { loader: 'style-loader' } // Use style-loader until MiniCssExtractPlugin supports hot reload

config.devServer = {
  publicPath: config.output.publicPath,
  hot: true,
  inline: true,
  compress: true,
  historyApiFallback: true,
  disableHostCheck: true,
  headers: {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
    'Access-Control-Allow-Headers': 'X-Requested-With, content-type, Authorization',
  }
}

module.exports = config
