import LocalizedStrings from 'react-localization'

const Strings = new LocalizedStrings({
  per: {
    systemTitle: 'سامانه‌ی پاداش و تنبیه کارکنان',
    shouldLogin: 'برای استفاده از سامانه، لازم است ابتدا به سامانه وارد شوید.',
    loginToSystem: 'ورود به سامانه',
    email: 'پست الکترونیکی',
    pleaseEnterEmail: 'لطفا پست الکترونیکی را وارد کنید!',
    password: 'کلمه‌ی عبور',
    pleaseEnterPassword: 'لطفا کلمه‌ی عبور را وارد کنید!',
    login: 'ورود',
    connectionError: 'خطا در اتصال',
    invalidCredentials: 'پست الکترونیکی یا کلمه‌ی عبور نادرست است!',
    welcomeSystem: 'گرامی، به سامانه‌ی تشویق و تنبیه کارکنان خوش آمدید!',
    yourRole: 'نقش شما در سامانه:',
    employee: 'کارمند',
    administrator: 'مدیر کل',
    useMenu: 'برای انجام  اعمال مختلف، لطفاً از فهرست سامانه استفاده کنید.',
    manageUsers: 'مدیریت کاربران',
    mainPage: 'صفحه‌ی اصلی',
    manageEvaluations: 'مدیریت ارزیابی‌ها',
    logout: 'خروج از سامانه',
    evaluationList: 'لیست ارزیابی‌ها',
    createNewEvaluation: 'ایجاد ارزیابی جدید',
    specifyThreshold: 'مشخص‌کردن آستانه‌ی تشویق و تنبیه',
    confirmLogout: 'تایید خروج از سامانه',
    areYouSureLogout: 'آیا واقعاً می‌خواهید از سامانه خارج شوید؟',
    title: 'عنوان',
    pleaseEnterTitle: 'لطفا عنوان را وارد کنید!',
    evaluationCreated: 'ارزیابی با موفقیت ایجاد شد.',
    sectionList: 'لیست بخش‌ها',
    createNewSection: 'ایجاد بخش جدید',
    enterSectionTitle: 'لطفا نام بخش جدید را وارد کنید:',
    sectionCreated: 'بخش با موفقیت ایجاد شد.',
    id: 'شماره',
    specifyThresholds: 'مشخص‌کردن آستانه‌ها',
    enterNewThresholds: 'لطفاً آستانه‌ها را مشخص کنید:',
    rewardThreshold: 'آستانه‌ی تشویق',
    punishmentThreshold: 'آستانه‌ی تنبیه',
    reward: 'تشویق',
    punishment: 'تنبیه',
    actions: 'اعمال',
    thresholdsSumitted: 'آستانه‌ها تعیین شدند.',
    delete: 'حذف',
    confirmDelete: 'تایید حذف',
    areYouSureDelete: 'آیا از حذف اطمینان دارید؟',
    users: 'کاربران',
    addUserDescription: 'برای اضافه‌کردن کاربر جدید روی دکمه‌ی زیر کلیک کنید!',
    addUser: 'افزودن کاربر',
    addUserInfo: 'لطفاً اطلاعات کاربری را برای کاربر جدید وارد کنید:',
    userName: 'نام کاربر',
    pleaseEnterUserName: 'لطفاً نام کاربر را وارد کنید!',
    section: 'بخش',
    pleaseEnterSection: 'لطفا بخش را مشخص کنید!',
    sectionModerator: 'مدیر بخش',
    userCreated: 'کاربر با موفقیت ایجاد شد.',
    userAlreadyExists: 'این کاربر از قبل در سیستم موجود است.',
    manage: 'مدیریت',
    managingEvaluation: 'مدیریت ارزیابی',
    return: 'بازگشت',
    specifyPolicy: 'تعیین سیاست',
    manageCriterions: 'مدیریت معیار‌ها',
    policyUpdated: 'سیاست‌ها به‌روز شدند.',
    evaluation: 'ارزیابی',
    addQualitativeCriterion: 'ایجاد معیار کیفی',
    addQuantativeCriterion: 'ایجاد معیار کمّی',
    weight: 'وزن',
    pleaseEnterWeight: 'لطفاً وزن را وارد کنید!',
    minValue: 'کم‌ترین مقدار',
    pleaseEnterMinValue: 'لطفاً کم‌ترین مقدار را وارد کنید!',
    maxValue: 'بیش‌ترین مقدار',
    pleaseEnterMaxValue: 'لطفاً بیش‌ترین مقدار را وارد کنید!',
    criterionAdded: 'معیار اضافه شد.',
    values: 'مقادیر',
    pleaseEnterValues: 'لطفاً مقادیر را وارد کنید!',
    grade: 'امتیاز',
    add: 'افزودن',
    between: 'بین',
    and: 'و',
    usersList: 'لیست کاربران',
    assignEvaluator: 'تعیین ارزیاب',
    evaluator: 'ارزیاب',
    assigneeAdded: 'ارزیاب اضافه شد.',
    myEvaluations: 'ارزیابی‌های من',
    evaluateOthers: 'ارزیابی دیگران',
    evaluatee: 'ارزیابی‌شونده',
    doEvaluation: 'انجام ارزیابی',
    viewEvaluation: 'مشاهده‌ی ارزیابی',
    for: 'برای',
    pleaseEnterValue: 'لطفاً مقدار را وارد کنید!',
    finalSubmit: 'ثبت نهایی',
    reportSubmitted: 'ارزیابی ثبت شد.',
    notHave: 'ندارد',
    reportResult: 'نتیجه‌ی ارزیابی',
    finalScore: 'امتیاز نهایی',
    allResults: 'همه‌ی نتایج',
    thresholdsWrong: 'آستانه‌ی تشویق باید بالاتر از یا مساوی با آستانه‌ی تنبیه باشد!',
  },
})

export default Strings
