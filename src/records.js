import { Record } from 'immutable'

export const SectionRecord = Record({
  id: 0,
  title: '',
})

export const UserRecord = Record({
  id: '',
  name: '',
  email: '',
  isSuperuser: false,
  section: SectionRecord(),
  isSectionManager: false,
})

export const AuthRecord = Record({
  token: '',
  user: UserRecord(),
})

export const AppRecord = Record({
  rehydrated: false,
})

export const StateRecord = Record({
  auth: AuthRecord(),
  app: AppRecord(),
})
