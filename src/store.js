import { createStore, applyMiddleware, compose } from 'redux'
import { createLogger } from 'redux-logger'
import thunkMiddleware from 'redux-thunk'
import { composeWithDevTools } from 'redux-devtools-extension'
import { createTransform, persistStore, autoRehydrate } from 'redux-persist-immutable'
import localForage from 'localforage'
import rootReducer from './reducers/index'
import { StateRecord } from './records'
import * as AppActions from './actions/app'

const initialState = StateRecord()

const loggerMiddleware = createLogger()

const enhancers = (process.env.NODE_ENV === 'production')
  ? compose(applyMiddleware(thunkMiddleware), autoRehydrate())
  : composeWithDevTools(applyMiddleware(thunkMiddleware, loggerMiddleware), autoRehydrate())

const store = createStore(rootReducer, initialState, enhancers)

function persistTransform(state, key) {
  return state
}

localForage.config({
  name: 'RewardAndPunish',
  version: 1.0,
  storeName: 'state',
  description: 'Reward and punish data store',
})

const transform = createTransform(persistTransform, persistTransform)

persistStore(store, { storage: localForage, transforms: [transform], blacklist: ['app'], keyPrefix: 'ood:' },
  () => { store.dispatch(AppActions.setRehydrated(true)) })

if (module.hot) {
  module.hot.accept('./reducers/', () => {
    const nextRootReducer = require('./reducers/index').default
    store.replaceReducer(nextRootReducer)
  })
}

export default store
