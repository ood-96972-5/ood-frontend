export function loginUser(token, user) {
  return { type: 'AUTH/LOGIN_USER', token, user }
}

export function logoutUser() {
  return { type: 'AUTH/LOGOUT_USER' }
}
