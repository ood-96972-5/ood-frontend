export function setRehydrated(value) {
  return { type: 'APP/SET_REHYDRATED', value }
}
