import 'babel-polyfill'
import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { BrowserRouter } from 'react-router-dom'
import { LocaleProvider } from 'antd'
import faIR from 'antd/lib/locale-provider/fa_IR'
import App from './components/App'
import '../stylesheet/index.less'

import store from './store'

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <LocaleProvider locale={faIR}>
        <App/>
      </LocaleProvider>
    </BrowserRouter>
  </Provider>, document.getElementById('root'))
