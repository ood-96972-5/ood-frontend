import { message } from 'antd'
import fetch from 'isomorphic-fetch'

import { BACKEND_URL } from './config'
import Store from './store'
import Strings from './localization'

export function toEnglishDigits(string) {
  return string
    .replace(/[٠١٢٣٤٥٦٧٨٩]/g, c => c.charCodeAt(0) - 1632)
    .replace(/[۰۱۲۳۴۵۶۷۸۹]/g, c => c.charCodeAt(0) - 1776)
}

export function toPersianDigits(string) {
  const persians = '۰۱۲۳۴۵۶۷۸۹'
  return string.replace(/[0-9]/g, englishDigit => persians[englishDigit])
}

export async function backendRequest(method, path, body) {
  try {
    const options = {
      method,
      credentials: 'same-origin',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
    }
    if (body) options.body = JSON.stringify(body)
    const token = Store.getState().getIn(['auth', 'token'])
    if (token) options.headers['Authorization'] = token
    return await fetch(`${BACKEND_URL}${path}`, options)
  } catch (e) {
    message.error(Strings.connectionError)
  }
}
