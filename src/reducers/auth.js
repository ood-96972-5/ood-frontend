import { AuthRecord, UserRecord, SectionRecord } from '../records'

function authReducer(state = AuthRecord(), action) {
  switch (action.type) {
  case 'AUTH/LOGIN_USER':
    action.user.section = SectionRecord(action.user.sectionAssignment.section)
    action.user.isSectionManager = action.user.sectionAssignment.isManager
    delete action.user.sectionAssignment
    return state
      .set('token', action.token)
      .set('user', UserRecord(action.user))
  case 'AUTH/LOGOUT_USER':
    return state.set('token', '').set('user', UserRecord())
  default: return state
  }
}

export default authReducer
