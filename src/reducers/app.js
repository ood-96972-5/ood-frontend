import { AppRecord } from '../records'

function appReducer(state = AppRecord(), action) {
  switch (action.type) {
  case 'APP/SET_REHYDRATED':
    return state.set('rehydrated', action.value)
  default:
    return state
  }
}

export default appReducer
