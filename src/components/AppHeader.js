import React from 'react'

import Strings from '../localization.js'

import Logo from '../../static/images/logo.jpg'

class AppHeader extends React.Component {
  render() {
    return <div className='app__header'>
      <img className='app__header__logo' src={Logo} />
      <h2 className='app__header__title'>{Strings.systemTitle}</h2>
    </div>
  }
}

export default AppHeader
