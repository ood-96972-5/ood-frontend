import React from 'react'
import { Switch, Route } from 'react-router-dom'
import { Layout } from 'antd'

import AppMenu from './AppMenu'
import Welcome from './panel/Welcome'
import Evaluations from './panel/Evaluations'
import Criterions from './panel/Criterions'
import ManageUser from './panel/ManageUser'
import ReviewList from './panel/ReviewList'

class UserPanel extends React.Component {
  render() {
    return <Layout.Content>
      <Layout style={{ padding: '24px 0' }}>
        <Layout.Sider width={200} breakpoint='lg' collapsedWidth='0'>
          <AppMenu authActions={this.props.authActions} history={this.props.history} user={this.props.user} />
        </Layout.Sider>
        <Layout.Content style={{ padding: '0 24px', minHeight: 280 }}>
          <Switch>
            <Route exact path='/panel' render={() => <Welcome user={this.props.user} />}/>
            <Route path='/panel/myEvaluations' render={() => <ReviewList mine user={this.props.user} />}/>
            <Route path='/panel/evaluateOthers' render={() => <ReviewList user={this.props.user} />}/>
            <Route path='/panel/allResults' render={() => <ReviewList mine all user={this.props.user} />}/>
            <Route path='/panel/evaluations' component={Evaluations}/>
            <Route path='/panel/criterions' render={() => <Criterions user={this.props.user} />}/>
            <Route path='/panel/manageUser' component={ManageUser}/>
          </Switch>
        </Layout.Content>
      </Layout>
    </Layout.Content>
  }
}

export default UserPanel
