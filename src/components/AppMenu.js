import React from 'react'
import { Menu, Icon, Modal } from 'antd'

import Strings from '../localization.js'

class AppMenu extends React.Component {
  getSelectedKey() {
    switch (location.pathname) {
    case '/panel':
      return '1'
    case '/panel/myEvaluations':
      return '2'
    case '/panel/evaluateOthers':
      return '3'
    case '/panel/allResults':
      return '4'
    case '/panel/evaluations':
      return '5'
    case '/panel/criterions':
      return '6'
    case '/panel/manageUser':
      return '7'
    }
  }

  logout() {
    Modal.confirm({
      title: Strings.confirmLogout,
      content: Strings.areYouSureLogout,
      onOk: () => {
        this.props.authActions.logoutUser()
        this.props.history.push('/login')
      },
    })
  }

  render() {
    return (
      <Menu
        selectedKeys={[this.getSelectedKey()]}
        mode='inline'
      >
        <Menu.Item key='1' onClick={() => this.props.history.push('/panel')}>
          <Icon type='desktop' />
          <span>{Strings.mainPage}</span>
        </Menu.Item>
        <Menu.Item key='2' onClick={() => this.props.history.push('/panel/myEvaluations')}>
          <Icon type='calculator' />
          <span>{Strings.myEvaluations}</span>
        </Menu.Item>
        <Menu.Item key='3' onClick={() => this.props.history.push('/panel/evaluateOthers')}>
          <Icon type='schedule' />
          <span>{Strings.evaluateOthers}</span>
        </Menu.Item>
        {this.props.user.get('isSuperuser') &&
          <Menu.Item key='4' onClick={() => this.props.history.push('/panel/allResults')}>
            <Icon type='calculator' />
            <span>{Strings.allResults}</span>
          </Menu.Item>
        }
        {this.props.user.get('isSuperuser') &&
          <Menu.Item key='5' onClick={() => this.props.history.push('/panel/evaluations')}>
            <Icon type='pie-chart' />
            <span>{Strings.manageEvaluations}</span>
          </Menu.Item>
        }
        {(this.props.user.get('isSuperuser') || this.props.user.get('isSectionManager')) &&
          <Menu.Item key='6' onClick={() => this.props.history.push('/panel/criterions')}>
            <Icon type='bars' />
            <span>{Strings.manageCriterions}</span>
          </Menu.Item>
        }
        {this.props.user.get('isSuperuser') &&
          <Menu.Item key='7' onClick={() => this.props.history.push('/panel/manageUser')}>
            <Icon type='user' />
            <span>{Strings.manageUsers}</span>
          </Menu.Item>
        }
        <Menu.Item key='8' onClick={() => this.logout()}>
          <Icon type='logout' />
          <span>{Strings.logout}</span>
        </Menu.Item>
      </Menu>
    )
  }
}

export default AppMenu
