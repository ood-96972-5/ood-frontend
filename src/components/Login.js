import React from 'react'
import { Card, Form, Icon, Input, Button, Layout, message } from 'antd'

import Strings from '../localization.js'
import { backendRequest } from '../utils'

class Login extends React.Component {
  handleSubmit = (e) => {
    e.preventDefault()
    this.props.form.validateFields(async (err, values) => {
      if (!err) {
        const response = await backendRequest('POST', '/api/users/login', values)
        if (response) {
          if (response.status === 200) {
            const json = await response.json()
            const token = json.token
            const user = json.worker
            this.props.authActions.loginUser(token, user)
            this.props.history.push('/panel')
          } else {
            message.error(Strings.invalidCredentials)
          }
        }
      }
    })
  }

  render() {
    const { getFieldDecorator } = this.props.form

    return <Layout.Content className='login'>
      <p className='login__text'>{Strings.shouldLogin}</p>
      <Card className='login__card' title={Strings.loginToSystem}>
        <Form onSubmit={this.handleSubmit}>
          <Form.Item>
            {getFieldDecorator('email', {
              rules: [
                { type: 'email', message: Strings.pleaseEnterEmail },
                { required: true, message: Strings.pleaseEnterEmail },
              ],
            })(
              <Input suffix={<Icon type='user' style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder={Strings.email} />
            )}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator('password', {
              rules: [{ required: true, message: Strings.pleaseEnterPassword }],
            })(
              <Input suffix={<Icon type='lock' style={{ color: 'rgba(0,0,0,.25)' }} />} type='password' placeholder={Strings.password} />
            )}
          </Form.Item>
          <Form.Item className='login__submit'>
            <Button type='primary' htmlType='submit' className='login__submit__button'>
              {Strings.login}
            </Button>
          </Form.Item>
        </Form>
      </Card>
    </Layout.Content>
  }
}

export default Form.create()(Login)
