import React from 'react'
import { Switch, Route, Redirect, withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Layout, Divider } from 'antd'

import AppHeader from './AppHeader'
import Login from './Login'
import UserPanel from './UserPanel'

import * as AuthActions from '../actions/auth'

class App extends React.Component {
  render() {
    const { state, authActions } = this.props
    const isLoggedIn = !!state.getIn(['auth', 'token'])

    return state.getIn(['app', 'rehydrated']) &&
      <Layout className='app__layout'>
        <Layout.Header><AppHeader /></Layout.Header>
        <Divider />
        <Switch>
          <Route path='/login' render={() => <Login authActions={authActions} history={this.props.history} />} />
          {isLoggedIn &&
            <Route path='/panel' render={() => <UserPanel authActions={authActions} history={this.props.history} user={state.getIn(['auth', 'user'])} />} />
          }
          {isLoggedIn
            ? <Redirect exact from='/' to='/panel' />
            : <Redirect to='/login' />
          }
        </Switch>
      </Layout>
  }
}

function mapStateToProps(state) { return { state } }
function mapDispatchToProps(dispatch) {
  return {
    authActions: bindActionCreators(AuthActions, dispatch),
  }
}

const WrappedApp = withRouter(connect(mapStateToProps, mapDispatchToProps)(App))

export default WrappedApp
