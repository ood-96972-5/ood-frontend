import React from 'react'
import { List } from 'immutable'
import { Button, Checkbox, Input, Modal, Table, Form, Select, message } from 'antd'
import { backendRequest } from '../../utils'

import Strings from '../../localization.js'

class ManageUser extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      addSectionShown: false,
      sections: List(),
      users: List(),
      sectionName: '',
      addUserShown: false,
    }
  }

  componentWillMount() {
    this.refreshSections()
    this.refreshUsers()
  }

  async refreshSections() {
    const response = await backendRequest('GET', '/api/sections')
    if (response) {
      this.setState({ sections: List(await response.json()) })
    }
  }

  async refreshUsers() {
    const response = await backendRequest('GET', '/api/users')
    if (response) {
      this.setState({ users: List(await response.json()) })
    }
  }

  async createNewSection() {
    const title = this.state.sectionName
    if (title) {
      const response = await backendRequest('POST', '/api/sections', { title })
      if (response) {
        if (response.status === 200) {
          const section = await response.json()
          this.setState({ sections: this.state.sections.push(section) })
          message.success(Strings.sectionCreated)
        } else message.error(Strings.connectionError)
        this.setState({ addSectionShown: false, sectionName: '' })
      }
    } else this.setState({ addSectionShown: false, sectionName: '' })
  }

  addNewUser() {
    this.props.form.validateFields(async (err, values) => {
      if (!err) {
        const data = {
          name: values.userName,
          email: values.email,
          password: values.password,
          isManager: !!values.moderator,
          sectionId: values.section,
        }
        const response = await backendRequest('POST', '/api/users/', data)
        if (response) {
          if (response.status === 200) {
            this.setState({ users: this.state.users.push(await response.json()) })
            this.setState({ addUserShown: false })
            this.props.form.resetFields()
            message.success(Strings.userCreated)
          } else if (response.status === 409) {
            message.error(Strings.userAlreadyExists)
          } else {
            message.error(Strings.connectionError)
          }
        }
      }
    })
  }

  renderSections() {
    const columns = [
      { title: Strings.id, dataIndex: 'id', key: 'id', align: 'center' },
      { title: Strings.title, dataIndex: 'title', key: 'title', align: 'center' },
    ]

    return (
      <div>
        <h3>{Strings.sectionList}</h3>
        <Table
          columns={columns}
          rowKey='id'
          dataSource={this.state.sections.toArray()}
        />
        <Modal
          title={Strings.createNewSection}
          visible={this.state.addSectionShown}
          onOk={() => this.createNewSection()}
          onCancel={() => this.setState({ addSectionShown: false, sectionName: '' })}
        >
          <p>{Strings.enterSectionTitle}</p>
          <Input
            value={this.state.sectionName}
            onChange={e => this.setState({ sectionName: e.target.value })}
          />
        </Modal>
        <Button
          type='primary'
          onClick={() => this.setState({ addSectionShown: true })}
        >
          {Strings.createNewSection}
        </Button>
      </div>
    )
  }

  renderUsers() {
    const columns = [
      { title: Strings.id, dataIndex: 'id', key: 'id', align: 'center' },
      { title: Strings.userName, dataIndex: 'name', key: 'userName', align: 'center' },
      { title: Strings.email, dataIndex: 'email', key: 'email', align: 'center' },
      { title: Strings.section, dataIndex: 'sectionAssignment', key: 'section', align: 'center', render: sectionAssignment => sectionAssignment.section.title },
    ]

    return (
      <div style={{ marginTop: '16px' }}>
        <h3>{Strings.usersList}</h3>
        <Table
          columns={columns}
          rowKey='id'
          dataSource={this.state.users.toArray()}
        />
      </div>
    )
  }

  renderAddUser() {
    const { getFieldDecorator } = this.props.form

    return <div style={{ marginTop: '16px' }}>
      <Button type='primary' onClick={() => this.setState({ addUserShown: true })}>{Strings.addUser}</Button>
      <Modal
        title={Strings.addUser}
        visible={this.state.addUserShown}
        onOk={() => this.addNewUser()}
        onCancel={() => this.setState({ addUserShown: false })}
      >
        <p>{Strings.addUserInfo}</p>
        <Form>
          <Form.Item label={Strings.userName}>
            {getFieldDecorator('userName', {
              rules: [
                { required: true, message: Strings.pleaseEnterEmail },
              ],
            })(
              <Input />
            )}
          </Form.Item>
          <Form.Item label={Strings.email}>
            {getFieldDecorator('email', {
              rules: [
                { type: 'email', message: Strings.pleaseEnterEmail },
                { required: true, message: Strings.pleaseEnterEmail },
              ],
            })(
              <Input />
            )}
          </Form.Item>
          <Form.Item label={Strings.password}>
            {getFieldDecorator('password', {
              rules: [{ required: true, message: Strings.pleaseEnterPassword }],
            })(
              <Input type='password' />
            )}
          </Form.Item>
          <Form.Item label={Strings.section}>
            {getFieldDecorator('section', {
              rules: [{ required: true, message: Strings.pleaseEnterSection }],
            })(
              <Select>
                {this.state.sections.map(section =>
                  <Select.Option key={section.id} value={section.id}>{section.title}</Select.Option>
                )}
              </Select>
            )}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator('moderator', {
              valuePropName: 'checked',
            })(
              <Checkbox>{Strings.sectionModerator}</Checkbox>
            )}
          </Form.Item>
        </Form>
      </Modal>
    </div>
  }

  render() {
    return <div>
      {this.renderSections()}
      {this.renderUsers()}
      {this.renderAddUser()}
    </div>
  }
}

export default Form.create()(ManageUser)
