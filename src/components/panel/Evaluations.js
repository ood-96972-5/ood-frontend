import React from 'react'
import { List, Map } from 'immutable'
import { Button, Modal, Table, Select, message } from 'antd'

import { backendRequest } from '../../utils'
import Strings from '../../localization.js'

import AddEvaluation from './Evaluations/AddEvaluation'
import SpecifyPolicy from './Evaluations/SpecifyPolicy'

class Evaluations extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      sections: Map(),
      addModalShown: false,
      specifyModalShown: false,
      assignModalShown: false,
      assignee: null,
      evaluations: Map(),
      users: List(),
      evaluationId: null,
    }
  }

  async componentWillMount() {
    await this.refreshSections()
    await this.refreshEvaluations()
    await this.refreshUsers()
  }

  async refreshSections() {
    const response = await backendRequest('GET', '/api/sections')
    if (response) {
      const sections = await response.json()
      const sectionsMap = sections.map(section => [section.id, section.title])
      this.setState({ sections: Map(sectionsMap) })
    }
  }

  async refreshEvaluations() {
    const response = await backendRequest('GET', '/api/evaluation')
    if (response) {
      const evaluations = await response.json()
      const evaluationsMap = evaluations.map(evaluation => [evaluation.id, evaluation])
      this.setState({ evaluations: Map(evaluationsMap) })
    }
  }

  async refreshUsers() {
    const response = await backendRequest('GET', '/api/users')
    if (response) {
      this.setState({ users: List(await response.json()) })
    }
  }

  createNewEvaluation = async (sectionId, title) => {
    const response = await backendRequest('POST', '/api/evaluation', { title, sectionId })
    if (response) {
      if (response.status === 200) {
        const evaluation = await response.json()
        this.setState({ evaluations: this.state.evaluations.set(evaluation.id, evaluation) })
        message.success(Strings.evaluationCreated)
      } else message.error(Strings.connectionError)
      this.setState({ addModalShown: false, evaluationName: '' })
    }
  }

  submitPolicy = async (rewardThreshold = null, punishmentThreshold = null, reward = null, punishment = null) => {
    const { evaluationId } = this.state
    if (!evaluationId) return

    if (rewardThreshold < punishmentThreshold) {
      message.error(Strings.thresholdsWrong)
      return
    }

    const thresholdPayload = {
      evaluationId,
      punishmentThreshold,
      rewardThreshold,
    }

    const actionsPayload = {
      evaluationId,
      reward,
      punishment,
    }

    const thresholdResponse = await backendRequest('POST', '/api/evaluation/threshold', thresholdPayload)
    if (thresholdResponse && thresholdResponse.status === 200) {
      const newEvaluation = await thresholdResponse.json()
      this.setState({
        evaluations: this.state.evaluations.set(newEvaluation.id, newEvaluation),
      })
      const actionsResponse = await backendRequest('POST', '/api/evaluation/actions', actionsPayload)
      if (actionsResponse && actionsResponse.status === 200) {
        const newEvaluation = await actionsResponse.json()
        this.setState({
          evaluations: this.state.evaluations.set(newEvaluation.id, newEvaluation),
        })
        message.success(Strings.policyUpdated)
        this.setState({ specifyModalShown: false })
      } else message.error(Strings.connectionError)
    } else message.error(Strings.connectionError)
  }

  addAssignee = async () => {
    const { evaluationId } = this.state
    if (!evaluationId) return

    const payload = {
      evaluationId,
      reviewerId: this.state.assignee,
    }

    const { sectionId } = this.state.evaluations.get(evaluationId)
    this.state.users
      .filter(user => user.sectionAssignment.section.id === sectionId)
      .forEach(async user => {
        const userPayload = Object.assign(payload, { revieweeId: user.id })
        const response = await backendRequest('POST', '/api/reports', userPayload)
        if (!(response && response.status === 200)) message.error(Strings.connectionError)
      })

    this.setState({ assignModalShown: false })
    message.success(Strings.assigneeAdded)
  }

  renderAssignment() {
    return <Modal
      title={Strings.assignEvaluator}
      visible={this.state.assignModalShown}
      onCancel={() => this.setState({ assignModalShown: false })}
      onOk={this.addAssignee}
    >
      <div>
        {Strings.evaluator}:
        <Select style={{ width: 200, marginRight: '16px' }} onChange={id => this.setState({ assignee: id })}>
          {this.state.users.toJS().map(user =>
            <Select.Option key={user.id} value={parseInt(user.id)}>{user.name}</Select.Option>
          )}
        </Select>
      </div>
    </Modal>
  }

  renderTable() {
    const columns = [
      { title: Strings.id, dataIndex: 'id', key: 'id', align: 'center' },
      { title: Strings.title, dataIndex: 'title', key: 'title', align: 'center' },
      { title: Strings.section, dataIndex: 'sectionId', key: 'sectionId', align: 'center', render: sectionId => this.state.sections.get(sectionId) },
      {
        title: Strings.actions,
        dataIndex: '',
        key: 'actions',
        align: 'center',
        render: evaluation =>
          <div>
            <a onClick={() => this.setState({ specifyModalShown: true, evaluationId: evaluation.id })}>{Strings.specifyPolicy}</a>
            &nbsp;
            <a style={{ marginRight: '8px' }} onClick={() => this.setState({ assignModalShown: true, evaluationId: evaluation.id })}>{Strings.assignEvaluator}</a>
          </div>,
      },
    ]

    return (
      <div>
        <Table
          columns={columns}
          rowKey='id'
          dataSource={this.state.evaluations.toArray()}
        />
      </div>
    )
  }

  render() {
    return <div>
      <h3>{Strings.evaluationList}</h3>
      {this.renderTable()}
      {this.renderAssignment()}
      <AddEvaluation
        shown={this.state.addModalShown}
        sections={this.state.sections}
        onSubmit={this.createNewEvaluation}
        onCancel={() => this.setState({ addModalShown: false })}
      />
      <SpecifyPolicy
        shown={this.state.specifyModalShown}
        evaluation={this.state.evaluationId && this.state.evaluations.get(this.state.evaluationId)}
        onSubmit={this.submitPolicy}
        onCancel={() => this.setState({ specifyModalShown: false })}
      />
      <Button
        type='primary'
        style={{ marginTop: '16px' }}
        onClick={() => this.setState({ addModalShown: true })}
      >
        {Strings.createNewEvaluation}
      </Button>
    </div>
  }
}

export default Evaluations
