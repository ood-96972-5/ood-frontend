import React from 'react'
import { Map } from 'immutable'
import { Button, Table, Select, message } from 'antd'

import { backendRequest } from '../../utils'
import Strings from '../../localization.js'

import AddQuantative from './Criterions/AddQuantative'
import AddQualitative from './Criterions/AddQualitative'

class Criterions extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      sections: Map(),
      addQuantativeShown: false,
      addQualitativeShown: false,
      evaluations: Map(),
      currentSection: null,
      currentEvaluation: null,
    }
  }

  async componentWillMount() {
    await this.refreshSections()
  }

  async refreshSections() {
    if (this.props.user.get('isSuperuser')) {
      const response = await backendRequest('GET', '/api/sections')
      if (response) {
        const sections = await response.json()
        const sectionsMap = sections.map(section => [section.id, section.title])
        this.setState({ sections: Map(sectionsMap) })
      }
    } else {
      this.setState({
        sections: Map([[
          this.props.user.getIn(['section', 'id']),
          this.props.user.getIn(['section', 'title']),
        ]]),
      })
    }
  }

  async refreshEvaluations(sectionId) {
    const response = await backendRequest('GET', `/api/evaluation/section/${sectionId}`)
    if (response) {
      const evaluations = await response.json()
      const evaluationsMap = evaluations.map(evaluation => [evaluation.id, evaluation])
      this.setState({ evaluations: Map(evaluationsMap) })
    }
  }

  addNewQualitative = async (title, weight, grades) => {
    if (this.state.currentEvaluation) {
      const response = await backendRequest('POST', '/api/evaluation/criterion/qualitative', {
        evaluationId: this.state.currentEvaluation,
        title,
        weight,
        grades,
      })
      if (response && response.status === 200) {
        const evaluation = await response.json()
        this.setState({ evaluations: this.state.evaluations.set(evaluation.id, evaluation) })
        this.setState({ addQualitativeShown: false })
        message.success(Strings.criterionAdded)
      } else message.error(Strings.connectionError)
    }
  }

  addNewQuantative = async (title, weight, minGrade, maxGrade) => {
    if (this.state.currentEvaluation) {
      const response = await backendRequest('POST', '/api/evaluation/criterion/quantitative', {
        evaluationId: this.state.currentEvaluation,
        title,
        weight,
        minGrade,
        maxGrade,
      })
      if (response && response.status === 200) {
        const evaluation = await response.json()
        this.setState({ evaluations: this.state.evaluations.set(evaluation.id, evaluation) })
        this.setState({ addQuantativeShown: false })
        message.success(Strings.criterionAdded)
      } else message.error(Strings.connectionError)
    }
  }

  formatGrading(grading) {
    if ('minGrade' in grading) {
      return `${Strings.between} ${grading.minGrade} ${Strings.and} ${grading.maxGrade}`
    } else if ('scores' in grading) {
      return grading.scores.join(' - ')
    }
  }

  renderTable() {
    const columns = [
      { title: Strings.id, dataIndex: 'id', key: 'id', align: 'center' },
      { title: Strings.title, dataIndex: 'title', key: 'title', align: 'center' },
      { title: Strings.weight, dataIndex: 'weight', key: 'weight', align: 'center' },
      {
        title: Strings.grade,
        dataIndex: 'grading',
        key: 'grading',
        align: 'center',
        render: this.formatGrading,
      },
    ]

    const evaluation = this.state.currentEvaluation ? this.state.evaluations.get(this.state.currentEvaluation) : null
    const criteria = evaluation && evaluation.criteria
    return (
      <div>
        <Table
          columns={columns}
          rowKey='id'
          dataSource={criteria || []}
        />
      </div>
    )
  }

  onSectionChange = async (sectionId) => {
    this.setState({ currentSection: sectionId })
    if (sectionId) await this.refreshEvaluations(sectionId)
  }

  render() {
    return <div>
      <h3>{Strings.manageCriterions}</h3>
      <div>
        {Strings.section}:
        <Select style={{ width: 200, marginRight: '16px' }} onChange={this.onSectionChange}>
          {Object.entries(this.state.sections.toJS()).map(([id, section]) =>
            <Select.Option key={id} value={parseInt(id)}>{section}</Select.Option>
          )}
        </Select>
      </div>
      <div style={{ margin: '16px 0' }}>
        {Strings.evaluation}:
        <Select style={{ width: 200, marginRight: '16px' }} onChange={id => this.setState({ currentEvaluation: id })}>
          {Object.values(this.state.evaluations.toJS())
            .filter(evaluation => evaluation.sectionId === this.state.currentSection)
            .map(evaluation =>
              <Select.Option key={evaluation.id} value={evaluation.id}>{evaluation.title}</Select.Option>
            )
          }
        </Select>
      </div>
      {this.renderTable()}
      <div style={{ marginTop: '16px' }}>
        <Button
          type='primary'
          onClick={() => this.setState({ addQuantativeShown: true })}
          disabled={!this.state.currentEvaluation}
        >
          {Strings.addQuantativeCriterion}
        </Button>
        <Button
          type='primary'
          onClick={() => this.setState({ addQualitativeShown: true })}
          style={{ marginRight: '16px' }}
          disabled={!this.state.currentEvaluation}
        >
          {Strings.addQualitativeCriterion}
        </Button>
      </div>
      <AddQuantative
        shown={this.state.addQuantativeShown}
        onCancel={() => this.setState({ addQuantativeShown: false })}
        onSubmit={this.addNewQuantative}
      />
      <AddQualitative
        shown={this.state.addQualitativeShown}
        onCancel={() => this.setState({ addQualitativeShown: false })}
        onSubmit={this.addNewQualitative}
      />
    </div>
  }
}

export default Criterions
