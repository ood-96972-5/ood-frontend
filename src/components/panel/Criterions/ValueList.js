import React from 'react'

import { Button, Input, InputNumber, Table } from 'antd'

import Strings from '../../../localization.js'

class ValueList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      title: '',
      grade: null,
    }
  }

  getValue() {
    return this.props.value || {}
  }

  addNewValue = () => {
    if (this.state.title && this.state.grade !== null && this.state.grade !== undefined) {
      this.props.onChange(Object.assign(this.getValue(), {
        [this.state.title]: this.state.grade,
      }))
      this.setState({ title: '', grade: null })
    }
  }

  removeValue(title) {
    const newValue = Object.assign({}, this.getValue())
    delete newValue[title]
    this.props.onChange(Object.keys(newValue).length ? newValue : null)
  }

  renderTable() {
    const data = Object.entries(this.getValue()).map(([title, grade]) => ({ title, grade }))

    const columns = [
      { title: Strings.title, dataIndex: 'title', key: 'title', align: 'center' },
      { title: Strings.grade, dataIndex: 'grade', key: 'grade', align: 'center' },
      {
        title: Strings.actions,
        dataIndex: '',
        key: 'actions',
        align: 'center',
        render: item =>
          <div>
            <a onClick={() => this.removeValue(item.title)}>{Strings.delete}</a>
          </div>,
      },
    ]

    return (
      <div>
        <Table
          columns={columns}
          rowKey='title'
          dataSource={data}
        />
      </div>
    )
  }

  render() {
    return (
      <div>
        <div>
          <Input
            style={{ width: '150px' }}
            value={this.state.title}
            placeholder={Strings.title}
            onChange={e => this.setState({ title: e.target.value })}
          />
          <InputNumber
            style={{ margin: '0 8px' }}
            value={this.state.grade}
            placeholder={Strings.grade}
            onChange={grade => this.setState({ grade })}
          />
          <Button onClick={this.addNewValue}>{Strings.add}</Button>
        </div>
        {this.renderTable()}
      </div>
    )
  }
}

export default ValueList
