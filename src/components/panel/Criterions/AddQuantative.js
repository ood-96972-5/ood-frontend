import React from 'react'

import { Input, InputNumber, Modal, Form } from 'antd'

import Strings from '../../../localization.js'

class AddQuantative extends React.Component {
  checkAndSubmit = () => {
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.props.onSubmit(values.title, values.weight, values.minValue, values.maxValue)
      }
    })
  }

  componentDidUpdate(prevProps) {
    if (!this.props.shown) this.props.form.resetFields()
  }

  render() {
    const { getFieldDecorator } = this.props.form

    return (
      <Modal
        title={Strings.addQuantativeCriterion}
        visible={this.props.shown}
        onOk={this.checkAndSubmit}
        onCancel={this.props.onCancel}
      >
        <Form>
          <Form.Item label={Strings.title}>
            {getFieldDecorator('title', {
              rules: [{ required: true, message: Strings.pleaseEnterTitle }],
            })(
              <Input />
            )}
          </Form.Item>
          <Form.Item label={Strings.weight}>
            {getFieldDecorator('weight', {
              rules: [{ required: true, message: Strings.pleaseEnterWeight }],
            })(
              <InputNumber />
            )}
          </Form.Item>
          <Form.Item label={Strings.minValue}>
            {getFieldDecorator('minValue', {
              rules: [{ required: true, message: Strings.pleaseEnterMinValue }],
            })(
              <InputNumber />
            )}
          </Form.Item>
          <Form.Item label={Strings.maxValue}>
            {getFieldDecorator('maxValue', {
              rules: [{ required: true, message: Strings.pleaseEnterMaxValue }],
            })(
              <InputNumber />
            )}
          </Form.Item>
        </Form>
      </Modal>
    )
  }
}

export default Form.create()(AddQuantative)
