import React from 'react'

import Strings from '../../localization.js'

class Welcome extends React.Component {
  getRole() {
    if (this.props.user.get('isSuperuser')) return Strings.administrator
    else if (this.props.user.get('isSectionManager')) return `${Strings.sectionModerator} ${this.props.user.getIn(['section', 'title'])}`
    else return Strings.employee
  }

  render() {
    return <div>
      <p>{this.props.user.get('name')} {Strings.welcomeSystem}</p>
      <p>{Strings.yourRole} {this.getRole()}</p>
      <p>{Strings.useMenu}</p>
    </div>
  }
}

export default Welcome
