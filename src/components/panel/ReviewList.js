import React from 'react'
import { Map } from 'immutable'
import { Button, InputNumber, Table, Form, Select, message } from 'antd'

import { backendRequest } from '../../utils'
import Strings from '../../localization.js'

class ReviewList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      reports: Map(),
      currentReport: null,
    }
  }

  async componentWillMount() {
    await this.refreshReports()
  }

  async componentDidUpdate(prevProps) {
    if (prevProps.mine !== this.props.mine || prevProps.all !== this.props.all) {
      await this.refreshReports()
      this.setState({ currentReport: null })
    }
  }

  async refreshReports() {
    const myUrl = `/api/reports/reviewee/${this.props.user.get('id')}`
    const othersUrl = `/api/reports/reviewer/${this.props.user.get('id')}`
    const allUrl = '/api/reports'
    const url = this.props.mine ? (this.props.all ? allUrl : myUrl) : othersUrl

    const response = await backendRequest('GET', url)
    if (response) {
      const reports = await response.json()
      const reportsMap = reports.map(report => [report.id, report])
      this.setState({ reports: Map(reportsMap) })
    }
  }

  submitForm = () => {
    this.props.form.validateFields(async (err, values) => {
      if (!err) {
        const response = await backendRequest('POST', '/api/reports/score', {
          reportId: this.state.currentReport,
          criterionScoreTextMap: values,
        })
        if (response && response.status === 200) {
          const report = await response.json()
          this.setState({ reports: this.state.reports.set(report.id, report) })
          message.success(Strings.reportSubmitted)
        } else message.error(Strings.connectionError)
      }
    })
  }

  renderTable() {
    const columns = [
      { title: Strings.id, dataIndex: 'id', key: 'id', align: 'center' },
      { title: Strings.evaluation, dataIndex: 'evaluation', key: 'evaluation', align: 'center', render: evaluation => evaluation.title },
      {
        title: this.props.mine ? Strings.evaluator : Strings.evaluatee,
        dataIndex: this.props.mine ? 'reviewer' : 'reviewee',
        key: this.props.mine ? 'reviewer' : 'reviewee',
        render: user => user.name,
        align: 'center',
      },
      {
        title: Strings.actions,
        dataIndex: '',
        key: 'actions',
        align: 'center',
        render: report =>
          <div>
            {report.evaluation.criteria.length > 0 && report.scores.length === 0 && !this.props.mine
              ? <a onClick={() => this.setState({ currentReport: report.id })}>{Strings.doEvaluation}</a>
              : ((report.scores.length !== 0) &&
                <a onClick={() => this.setState({ currentReport: report.id })}>{Strings.viewEvaluation}</a>
              )
            }
          </div>,
      },
    ]

    if (this.props.all) {
      columns[4] = columns[3]
      columns[3] = {
        title: Strings.evaluatee,
        dataIndex: 'reviewee',
        key: 'reviewee',
        render: user => user.name,
        align: 'center',
      }
    }

    return (
      <div>
        <Table
          columns={columns}
          rowKey='id'
          dataSource={Object.values(this.state.reports.toJS())}
        />
      </div>
    )
  }

  renderList() {
    return <div>
      <h3>{this.props.mine ? (this.props.all ? Strings.allResults : Strings.myEvaluations) : Strings.evaluateOthers}</h3>
      {this.renderTable()}
    </div>
  }

  renderCriteriaField = (criteria) => {
    const { getFieldDecorator } = this.props.form

    return <Form.Item label={criteria.title} key={criteria.id}>
      {getFieldDecorator(criteria.id.toString(), {
        rules: [{ required: true, message: Strings.pleaseEnterValue }],
      })(
        'minGrade' in criteria.grading
          ? <InputNumber min={parseInt(criteria.grading.minGrade)} max={parseInt(criteria.grading.maxGrade)}/>
          : <Select style={{ maxWidth: '200px' }}>
            {criteria.grading.scores.map(score =>
              <Select.Option key={score} value={score}>{score}</Select.Option>
            )}
          </Select>
      )}
    </Form.Item>
  }

  renderViewOnlyField(score) {
    const scoreValue = 'minGrade' in score.criterion.grading
      ? score.score
      : `${score.score} (${score.value})`
    return <div key={score.criterion.id} style={{ margin: '16px 0' }}>
      <span>{score.criterion.title}:</span>
      <span style={{ marginRight: '8px' }}>{scoreValue} * {score.criterion.weight}</span>
    </div>
  }

  renderSummary(report) {
    const score = report.scores.map(score => score.value * score.criterion.weight).reduce((acc, val) => acc + val)
    const reward = (report.rewarded && report.evaluation.thresholds.rewardAction) || Strings.notHave
    const punish = (report.punished && report.evaluation.thresholds.punishmentAction) || Strings.notHave

    return <div>
      <h3>{Strings.reportResult}</h3>
      <div style={{ margin: '16px 0' }}>
        <span>{Strings.finalScore}:</span>
        <span style={{ marginRight: '8px' }}>{score}</span>
      </div>
      <div style={{ margin: '16px 0' }}>
        <span>{Strings.reward}:</span>
        <span style={{ marginRight: '8px' }}>{reward}</span>
      </div>
      <div style={{ margin: '16px 0' }}>
        <span>{Strings.punishment}:</span>
        <span style={{ marginRight: '8px' }}>{punish}</span>
      </div>
    </div>
  }

  renderReportForm() {
    const report = this.state.reports.get(this.state.currentReport)
    const viewTitle = `${Strings.viewEvaluation} «${report.reviewer.name}» ${Strings.for} «${report.reviewee.name}»`
    const doTitle = `${Strings.doEvaluation} ${Strings.for} «${report.reviewee.name}»`
    const viewOnly = report.scores.length > 0

    return <div>
      <h3>{viewOnly ? viewTitle : doTitle} (<a onClick={() => this.setState({ currentReport: null })}>{Strings.return}</a>)</h3>
      {viewOnly
        ? <div>
          {report.scores.map(this.renderViewOnlyField)}
          {this.renderSummary(report)}
        </div>
        : <Form>
          {report.evaluation.criteria.map(this.renderCriteriaField)}
          <Button type='primary' onClick={this.submitForm}>{Strings.finalSubmit}</Button>
        </Form>
      }
    </div>
  }

  render() {
    return this.state.currentReport
      ? this.renderReportForm()
      : this.renderList()
  }
}

export default Form.create()(ReviewList)
