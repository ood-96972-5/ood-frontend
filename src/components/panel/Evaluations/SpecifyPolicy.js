import React from 'react'

import { Input, InputNumber, Modal, Form } from 'antd'

import Strings from '../../../localization.js'

class SpecifyPolicy extends React.Component {
  checkAndSubmit = () => {
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.props.onSubmit(values.rewardThreshold, values.punishmentThreshold, values.reward, values.punishment)
      }
    })
  }

  componentDidUpdate(prevProps) {
    if (!this.props.shown) this.props.form.resetFields()
    else if (this.props.shown && !prevProps.shown && this.props.evaluation) {
      this.props.form.setFields({
        rewardThreshold: { value: this.props.evaluation.thresholds.reward },
        punishmentThreshold: { value: this.props.evaluation.thresholds.punishment },
        reward: { value: this.props.evaluation.thresholds.rewardAction },
        punishment: { value: this.props.evaluation.thresholds.punishmentAction },
      })
    }
  }

  render() {
    const { getFieldDecorator } = this.props.form

    return (
      <Modal
        title={Strings.specifyPolicy}
        visible={this.props.shown}
        onOk={this.checkAndSubmit}
        onCancel={this.props.onCancel}
      >
        <Form>
          <Form.Item label={Strings.rewardThreshold}>
            {getFieldDecorator('rewardThreshold')(
              <InputNumber />
            )}
          </Form.Item>
          <Form.Item label={Strings.punishmentThreshold}>
            {getFieldDecorator('punishmentThreshold')(
              <InputNumber />
            )}
          </Form.Item>
          <Form.Item label={Strings.reward}>
            {getFieldDecorator('reward')(
              <Input />
            )}
          </Form.Item>
          <Form.Item label={Strings.punishment}>
            {getFieldDecorator('punishment')(
              <Input />
            )}
          </Form.Item>
        </Form>
      </Modal>
    )
  }
}

export default Form.create()(SpecifyPolicy)
