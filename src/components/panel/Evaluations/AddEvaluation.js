import React from 'react'

import { Input, Modal, Form, Select } from 'antd'

import Strings from '../../../localization.js'

class AddEvaluation extends React.Component {
  checkAndSubmit = () => {
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.props.onSubmit(parseInt(values.section), values.title)
      }
    })
  }

  componentDidUpdate() {
    if (!this.props.shown) this.props.form.resetFields()
  }

  render() {
    const { getFieldDecorator } = this.props.form

    return (
      <Modal
        title={Strings.createNewEvaluation}
        visible={this.props.shown}
        onOk={this.checkAndSubmit}
        onCancel={this.props.onCancel}
      >
        <Form>
          <Form.Item label={Strings.title}>
            {getFieldDecorator('title', {
              rules: [{ required: true, message: Strings.pleaseEnterTitle }],
            })(
              <Input />
            )}
          </Form.Item>
          <Form.Item label={Strings.section}>
            {getFieldDecorator('section', {
              rules: [{ required: true, message: Strings.pleaseEnterSection }],
            })(
              <Select>
                {Object.entries(this.props.sections.toJS()).map(([id, section]) =>
                  <Select.Option key={id} value={id}>{section}</Select.Option>
                )}
              </Select>
            )}
          </Form.Item>
        </Form>
      </Modal>
    )
  }
}

export default Form.create()(AddEvaluation)
