const path = require('path')
const webpack = require('webpack')
const CopyPlugin = require('copy-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const LessPluginAutoPrefix = require('less-plugin-autoprefix')
const MiniCssExtractPlugin = require("mini-css-extract-plugin")

module.exports = {
  context: __dirname,

  entry: {
    app: './src/index',
  },

  output: {
    path: path.resolve(__dirname, './dist'),
    publicPath: '/',
    filename: 'js/[name].[hash].js',
  },

  plugins: [
    new CopyPlugin([]),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(process.env.NODE_ENV),
        BACKEND_URL: JSON.stringify(process.env.BACKEND_URL),
      },
    }),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, './index.ejs'),
      inject: 'body',
    }),
    new webpack.ContextReplacementPlugin(/moment[/\\]locale$/, /en-gb|fa/),
    new MiniCssExtractPlugin({
      filename: "css/[name].[hash].css",
    }),
    new webpack.HashedModuleIdsPlugin()
  ],

  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: ['env', 'stage-0', 'react'],
              plugins: [["import", { "libraryName": "antd", "libraryDirectory": "es", "style": "css" }]],
            }
          }
        ],
      },
      {
        test: /(\.css)$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              sourceMap: true,
            },
          },
          { loader: 'postcss-loader' },
        ],
      },
      {
        test: /(\.less)$/,
        use: [
          MiniCssExtractPlugin.loader,
          { loader: 'css-loader' },
          {
            loader: 'less-loader',
            options: {
              sourceMap: true,
              plugins: [new LessPluginAutoPrefix()],
            },
          },
        ],
      },
      {
        test: /\.(jpg|png|woff|woff2|eot|ttf|svg|gif)$/,
        use: {
          loader: 'url-loader',
          query: {
            publicPath: '/',
            limit: 2000,
            name: 'static/[hash].[ext]',
          },
        },
      },
    ],
  },
}
