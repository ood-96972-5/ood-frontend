FROM nginx:1.15
ADD dist /app
ADD nginx/ood.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
