#!/bin/sh
npm install
export BACKEND_URL="${BACKEND_URL:-http://localhost:8080}"
npm run build
docker-compose up -d --build
